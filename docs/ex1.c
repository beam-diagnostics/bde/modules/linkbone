/*
Code can be compiled with wxDev C++
http://wxdsgn.sourceforge.net/ 

*/
//****************************************************//
//        LinkBone test program example               //
//        Coaxial switch with remote control example   //
//        www.linkbone.com                            //
//****************************************************//
 
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <windows.h>
#include <conio.h>
 
HANDLE connectToSwitch(char *com)
{
    HANDLE port;
    DCB portConfig;
    char portName[32];
    COMMTIMEOUTS timeout;
 
    // open serial port
    snprintf(portName,32, "\\\\.\\%s", com);
 
    port=CreateFileA(portName, GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING, 0, 0);
    if (port == INVALID_HANDLE_VALUE)
    {
        printf("Cannot open COM port");
        getch();
        return INVALID_HANDLE_VALUE;
    }
 
    // set transmission parameters to coaxial switch with remote serial port
    if (!GetCommState(port,&portConfig))
        return INVALID_HANDLE_VALUE;
 
    portConfig.BaudRate = CBR_9600;
    portConfig.StopBits = ONESTOPBIT;
    portConfig.Parity = NOPARITY;
    portConfig.ByteSize = 8; 
 
    if (!SetCommState(port,&portConfig))
        return INVALID_HANDLE_VALUE;
 
    // set the read/write timeouts to serial port of coaxial switch
    timeout.ReadIntervalTimeout = 1;
    timeout.ReadTotalTimeoutMultiplier = 1;
    timeout.ReadTotalTimeoutConstant = 1;
    timeout.WriteTotalTimeoutMultiplier = 1;
    timeout.WriteTotalTimeoutConstant = 1;
 
    if (!SetCommTimeouts(port, &timeout))
        return INVALID_HANDLE_VALUE;
 
    return port;
}
 
void sendToSwitch(HANDLE port,char cmd[])
{
    char bufferIn[100];
    DWORD length, done=0;
 
    // send data to serial port of coaxial switch
    WriteFile(port, cmd, strlen(cmd),&length, NULL);
    // wait for "Done." reply from coaxial switch
    while(done<sizeof("Done.\n"))
    {
        ReadFile(port, bufferIn,sizeof(bufferIn),&length,NULL);
        done+=length;
    }
}
 
////////////////////////////////////////////////////////////
// the main test program using coaxial switch with remote //
////////////////////////////////////////////////////////////
 
int main(int argc, char *argv[])
{
    //open to serial port connected to LinkBone coaxial switch
    HANDLE port = connectToSwitch("COM4");
    if (port==INVALID_HANDLE_VALUE) // if error exit the program
        return 0;
 
    //send control commands to LinkBone coaxial switch with remote control
    sendToSwitch(port, "reset\n");
    //multiplexer mode of switch box
    sendToSwitch(port, "mode single\n");    
 
    //enable A input/output path
    sendToSwitch(port, "on a\n");
    //doSomething();
 
    //enable B input/output path
    sendToSwitch(port, "on b\n");
    //doSomething();
 
    //close the port serial connection to LinkBone coaxial switch with remote control
    CloseHandle(port);
    printf("Test script completed. Press an key to continue.\n");
    getch();
    return 0;
}

